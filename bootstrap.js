/*global Components: false */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

"use strict";


/* global APP_SHUTDOWN: false */

Cu.importGlobalProperties(["XMLHttpRequest"]);

var gAllModules = [];

function install() {}

function uninstall() {}

function startup(data, reason) {
  try {
    let JSUnitCommandLine = ChromeUtils.import("chrome://jsunit/content/modules/jsunit-service.js").JSUnitCommandLine;
    JSUnitCommandLine.startup(reason);
  } catch (ex) {
    logException(ex);
  }
}

function shutdown(data, reason) {}


function logException(exc) {
  try {
    const Services = Cu.import("resource://gre/modules/Services.jsm").Services;
    Services.console.logStringMessage(exc.toString() + "\n" + exc.stack);
  } catch (x) {}
}